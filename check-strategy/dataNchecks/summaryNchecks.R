calcRatio = function(x,y,paired=TRUE,silent=TRUE){
  if(any(x<=0) | any(y<=0)){
    stop("x and y values must be greater than 0")
  }
  x = log(x)
  y = log(y)
  model = t.test(x,y,paired=paired)
  if(paired){
    ratio = exp(model$estimate)
  }else{
    ratio = exp(model$estimate[1]-model$estimate[2])
  }
  ratio = unname(ratio)
  l95 = exp(model$conf.int[1])
  u95 = exp(model$conf.int[2])
  if(!silent){
    cat("Ratio:",ratio,"\n")
    cat("95% CI:",l95,u95,"\n")
  }
  invisible(list(ratio=ratio,CI=c(l95,u95)))
}

path0 <- "/home/gcovarrubias"#"Z:"
path <- file.path(path0,"breeding-program-assessment/check-strategy/dataNchecks");path

# Analyze output for report
library(data.table, lib.loc = file.path(path0,"R_LIBS36")) #For quickly collapsing a list of data.frames
library(plyr, lib.loc = file.path(path0,"R_LIBS36")) #For summarizing multiple reps
# library(lme4)
# library(emmeans, lib.loc = file.path(path0,"R_LIBS36"))
library(asreml, lib.loc = file.path(path0,"R_LIBS36"))

#Read Data----
gg_color_hue <- function(n) {
  hues = seq(15, 375, length = n + 1)
  hcl(h = hues, l = 65, c = 100)[1:n]
}
'%!in%' <- function(x,y)!('%in%'(x,y)) 

colorVec = gg_color_hue(3)
scenarios = c(#"plant_1_checks_and_refresh_1_every_1_years",
  "plant_2_checks_and_refresh_1_every_1_years",
  "plant_4_checks_and_refresh_1_every_1_years",
  "plant_6_checks_and_refresh_1_every_1_years",
  "plant_8_checks_and_refresh_1_every_1_years",
  "plant_10_checks_and_refresh_1_every_1_years",
  "plant_12_checks_and_refresh_1_every_1_years",
  "plant_14_checks_and_refresh_1_every_1_years",
  "plant_16_checks_and_refresh_1_every_1_years",
  "plant_18_checks_and_refresh_1_every_1_years",
  "plant_20_checks_and_refresh_1_every_1_years"
)
reps = 191:200
rawData = vector("list",length(reps)*length(scenarios))
i = 0L
outpuList <- list()
for(SCENARIO in scenarios){ # SCENARIO <- scenarios[1]
  
  resList <- list()
  for(REP in reps){ # REP <- reps[1]
    print(paste(SCENARIO,"REP", REP))
    i = i+1L
    FILE = paste0(SCENARIO,"_",REP,".rds")
    
    path0 <- "/home/gcovarrubias"#"Z:"
    path <- file.path(path0,"breeding-program-assessment/check-strategy/dataNchecks");path
    
    tmp = try(readRDS(file.path(path,FILE)),silent = TRUE)
    
    path0 <- "/home/gcovarrubias"#"Z:"
    path <- file.path(path0,"breeding-program-assessment/check-strategy/dataNchecks");path
    
    if(class(tmp) != "try-error"){
      rawData <- tmp#outpuList[[i]] <- tmp[21:nrow(tmp),]
      
      # #Centering on final year of burn-in, accounts for rep effects
      
      gto=21:50  # years sampled
      sampleStage="Stage3"
      # REP=1 # iteration simulated
      useData <- rawData[which( (rawData$year %in% gto) & (rawData$stage %in% sampleStage)),] 
      head(useData)
      nrow(useData)
      with(useData, table(year, env))
      
      #########################
      ## TPE COVERAGE DECISION
      #########################
      # we subset to only considered environments
      keep <- list()
      for(iYear in unique(useData$year)){
        useEnvs <- sample(unique(as.character(useData$env)),4)
        keep[[iYear]]<-which(useData$env %in% useEnvs & useData$year == iYear)
      }; keep <- unlist(keep)
      # subset
      finalData <- droplevels(useData[keep,]) # keep only environments sampled
      # subset to material of interest (elite lines and checks, remove control population)
      finalDataFC <- droplevels(finalData[which(finalData$type %in% c("Forward", "Checks")),]) #  "Control"
      with(finalDataFC, table(year, env))
      nrow(finalDataFC)
      
      finalDataFCC <- droplevels(finalData[which(finalData$type %in% c("Forward", "Checks","Control")),]) #  
      ControlNames <- as.character(unique(finalDataFCC[which(finalDataFCC$type %in% "Control"),"id"]))
      ControlsToRemove <- ControlNames[-c(1:10)]
      finalDataFCC <- droplevels(finalDataFCC[which(finalDataFCC$id %!in% ControlsToRemove),])
      nrow(finalDataFCC)
      #####################################
      ## add missing variables for modeling
      #####################################
      finalDataFC$yearf <- as.factor(finalDataFC$year) # convert to factor
      finalDataFC$typef <- as.factor(finalDataFC$type) # convert to factor
      finalDataFC$fieldinst <- as.factor(paste0(finalDataFC$yearf, finalDataFC$env))
      finalDataFC$fieldinstn <- as.numeric(finalDataFC$fieldinst)
      finalDataFC$envid <- paste(finalDataFC$env, finalDataFC$id)
      finalDataFC$envtype <- paste(finalDataFC$env, finalDataFC$type)
      finalDataFC <- finalDataFC[with(finalDataFC, order(-fieldinstn)), ]
      
      finalDataFCC$yearf <- as.factor(finalDataFCC$year) # convert to factor
      finalDataFCC$typef <- as.factor(finalDataFCC$type) # convert to factor
      finalDataFCC$fieldinst <- as.factor(paste0(finalDataFCC$yearf, finalDataFCC$env))
      finalDataFCC$fieldinstn <- as.numeric(finalDataFCC$fieldinst)
      finalDataFCC$envid <- paste(finalDataFCC$env, finalDataFCC$id)
      finalDataFCC$envtype <- paste(finalDataFCC$env, finalDataFCC$type)
      finalDataFCC <- finalDataFCC[with(finalDataFCC, order(-fieldinstn)), ]
      
      ###################
      # estimated genetic gain
      ###################
      # with(finalDataFC, table(id,env))
      # library(asreml)
      
      
      # mix.var <- asreml(p~ year + yearf + env + id,
      #                   random= ~ year:id + env:id,
      #                   residual=~dsum(~units | fieldinst),
      #                   data=droplevels(finalDataFC), 
      #                   maxiter=50, workspace=250000000,trace=FALSE
      # )
      
      # mix.var <- try(asreml(p~ year + env + id,
      #                   random= ~ env:id,
      #                   residual=~units,
      #                   data=droplevels(finalDataFC), 
      #                   maxiter=50, workspace=250000000,trace=FALSE
      # ), silent = TRUE)
      head(finalDataFC)
      # mix.var <- try(lmer(p~ env + type + year + type:id +
      #                       (1|envid),
      #                     data=droplevels(finalDataFC)
      # ), silent = TRUE)
      # 
      # 
      # mix.var2 <- try(lmer(p~ env + type + year + type:id +
      #                        (1|envid),
      #                      data=droplevels(finalDataFCC)
      # ), silent = TRUE)
      
      mix.var <- try(asreml(p~ env + typef + year + typef:id ,
                            workspace=250000000,
                            # random= ~ env:id,
                            # residual=~units,
                            data=droplevels(finalDataFC)
      ), silent = TRUE)
      
      mix.var2 <- try(asreml(p~ env + typef + year + typef:id ,
                             workspace=250000000,
                             # random= ~ env:id,
                             # residual=~units,
                             data=droplevels(finalDataFCC)
      ), silent = TRUE)
      
      if(class(mix.var)=="try-error"){
        # mix.var <- try(asreml(p~ year + env + id,
        #                       # random= ~ env:id,
        #                       residual=~units,
        #                       data=droplevels(finalDataFC), 
        #                       maxiter=50, workspace=250000000,trace=FALSE
        # ), silent = TRUE)
        
        mix.var <- try(asreml(p~ env + typef + year + id ,
                              workspace=250000000,
                              # random= ~ env:id,
                              # residual=~units,
                              data=droplevels(finalDataFC)
        ), silent = TRUE)
        
        mix.var2 <- try(asreml(p~ env + typef + year + id ,
                               workspace=250000000,
                               # random= ~ env:id,
                               # residual=~units,
                               data=droplevels(finalDataFCC)
        ), silent = TRUE)
      }
      summary(mix.var)
      
      
      ### 3) Predict means and merge the year of origin
      base <- unique(finalDataFC[,c("id","yearOrigin","typef")]) # extract year of origin for materials to merge later
      predictions <- predict(mix.var, classify = "id", trace=FALSE, pworkspace=250000000, aliased = TRUE)# obtain across years and across environment means
      predictions2 <- predictions$pvals # extract adjusted means
      
      # pp <- emmeans(mix.var, ~id)
      # predictions2 <- as.data.frame(summary(pp))[c('id','emmean', 'SE')]; colnames(predictions2)[2:3] <- c("predicted.value","std.error")
      predictions3 <- merge(predictions2, base, by="id", all.x = TRUE) # add year of origin to adjusted means
      predictions3 <- predictions3[which(predictions3$typef == "Forward"),] # remove checks
      head(predictions3)
      
      predictions <- predict(mix.var2, classify = "id", trace=FALSE, pworkspace=250000000, aliased = TRUE)# obtain across years and across environment means
      predictions2 <- predictions$pvals # extract adjusted means
      # ppC <- emmeans(mix.var2, ~id)
      # predictions2 <- as.data.frame(summary(ppC))[c('id','emmean', 'SE')]; colnames(predictions2)[2:3] <- c("predicted.value","std.error")
      predictions4 <- merge(predictions2, base, by="id", all.x = TRUE) # add year of origin to adjusted means
      predictions4 <- predictions4[which(predictions4$typef == "Forward"),] # remove checks
      head(predictions4)
      # plot(predicted.value~yearOrigin,data=predictions3)
      
      ### 4) The response to selection and rate of gain
      R0 <- aggregate(predicted.value~yearOrigin,data=predictions3, FUN=mean)
      mean(diff(R0$predicted.value)) # mean response per year
      R0$predicted.value[nrow(R0)] - R0$predicted.value[1]# total response across years
      mix.lm.var2 <- lm(predicted.value~yearOrigin, data=predictions3) # fit the second model
      b2=summary(mix.lm.var2)$coefficients[2,1]; b2 # extract rate of genetic gain
      b2se=summary(mix.lm.var2)$coefficients[2,2]; 
      
      R0 <- aggregate(predicted.value~yearOrigin,data=predictions4, FUN=mean)
      mean(diff(R0$predicted.value)) # mean response per year
      R0$predicted.value[nrow(R0)] - R0$predicted.value[1]# total response across years
      mix.lm.var2 <- lm(predicted.value~yearOrigin, data=predictions4) # fit the second model
      b3=summary(mix.lm.var2)$coefficients[2,1]; b3# extract rate of genetic gain
      b3se=summary(mix.lm.var2)$coefficients[2,2]; 
      
      # true genetic gain
      realGainData <- droplevels(useData[which(useData$type %in% c("Forward")),]) # filter original data to only forward breeding material (no checks or control population)
      realGainData <- aggregate(g~id+yearOrigin,data=realGainData, FUN=mean) # aggregate by year of origin
      mix.var.true <- lm(g~yearOrigin, data=realGainData) # fit a model for true genetic gain
      b4=summary(mix.var.true)$coefficients[2,1]; b4 # extract true rate of genetic gain
      b4se=summary(mix.var.true)$coefficients[2,2];
      b2
      resList[[REP]] <- data.frame(estim=c(b2,b2se), estimC=c(b3,b3se), true=c(b4,b4se), type=c("GG","GGse"), scenario=SCENARIO, iteration=REP)
    }else{
      print("no data for this trt and rep")
    }
    
  }
  outpuList[[SCENARIO]] <- do.call(rbind, resList)
}

OUT <- do.call(rbind, outpuList)
head(OUT)

saveRDS(OUT,file=file.path(path,paste0("summaryNchecks",REP,".rds")))

