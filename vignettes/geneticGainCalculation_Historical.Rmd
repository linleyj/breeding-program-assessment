---
title: "Genetic gain calculation (historical data method)"
author: "Giovanny Covarrubias-Pazaran"
date: "`r Sys.Date()`"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{genetic gain historical}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

The methods explained in the Breeding program assessment manual of EiB are:

+ Expected/predicted (1)

+ Realized (2)

+ + Historical trials (2.1)
  
+ + + Good connectivity scenario (2.1.1) (**preferred method**)

+ + + Bad connectivity scenario (2.1.2)
     
+ + Era trials (2.2)
  
This vignette demonstrates how to calculate the realized rate of genetic gain using historical trial data that followed good experimental design practices and check conectivity and replacement recommendations proposed by EiB to maximize accuracy of the estimates (2.1.1). This represents the ideal state of breeding programs to measure the rate of genetic gain. The values coming from this type of analyses can be considered as KPI's to be shared with funding and collaborator institutions. 
  
## INTRODUCING THE EXAMPLE
  
  Assume a breeding program that has been runing for 20 years and that recycles every 5 years and that comprises the following testing stages:
  
  1) STAGE 1
  2) STAGE 2
  3) STAGE 3
  4) ELITE
  
  We have made available in the package 15 independent iterations of a simulated breeding program following the proposed structure using the same founding population in order to calculate and estimate the true and estimated genetic gain. The data that will be loaded has simulated that we have run a pipeline with the different stages of the breeding program that has been connected through checks. In this example we will pick the historical data of the last 20 years for the ELITE stage of testing. Each programs needs to take the decision of which testing stages or stages should be picked and the time window (here i.e. 20 years).
  
  To show how the calculation is done with this type of data we first load the packages that will be required to do the analysis and visualizations.
  
```{r, echo=FALSE, message=FALSE, warning=FALSE}
library(lme4)
library(ggplot2)
library(asreml)
# assuming you have downloaded the project folder in your Desktop we set it as the working directory. Modify if this is not the case
path0 <- "Z:"
path1 <- file.path(path0,"/breeding-program-assessment/")
```

## GERMPLASM SAMPLE AND TIME WINDOW DECISIONS

In practice the program needs to decide which stage or gemplasm sample and for which window of time the analysis should be performed. In this example we will then load 1 of the 15 simulated iterations available and assume we are interested in sampling the "ELITE" material that has been derived in the las "20 years" and that dta is captured in a database (i.e. EBS). 

```{r}
########################
# define time-window and 
# germplasm sample: "ELITE", "STAGE3", "STAGE2", "STAGE1"
########################
gto=21:40  # years sampled
sampleStage="Elite"
REP=1 # iteration simulated
load(file.path(path1,"simulated-data",paste0("BASE_",REP,".RData")))
# subset to take only the last 20 years
useData <- yearData[which((yearData$year %in% gto) & (yearData$stage %in% sampleStage)),] 
head(useData)
```

Notice that in the simulated data we have identifiers for individuals, pedigree, environments tested and a check indicator. In addition, we have true genotype, environment and genotype by environment effects, although in reality this is not avaialable and needs to be estimated. The simulations allow us to see how good is a method or strategy to estimate accurately these effects. In this case will be used to see how accurate the era trials are to estimate the true genetic gain.

## TPE COVERAGE DECISION

In practice the program needs to decide in which environments from the TPE the different germplasm stages will be tested to ensure that the TPE is well represented at different decision times. In this example we have simulated that historically the Elite trials have been growing in all possible environments of the TPE (in our simulation 20 20) but for the sake of illustrating the reality we will assume that only 40% of the available locations have been used to grow our ELITE material in 2 different years (20*2=40 environments; we sample 4 in each year).

```{r}
# 7 out of 20 environments were planted
with(useData, table(year, env))
```

And to emulate reality we will remove the data for those environments that we emulated that were not planted.

```{r}
# we subset to only considered environments
keep <- list()
for(iYear in unique(useData$year)){
  useEnvs <- sample(unique(as.character(useData$env)),4)
  keep[[iYear]]<-which(useData$env %in% useEnvs & useData$year == iYear)
}; keep <- unlist(keep)
# subset
finalData <- droplevels(useData[keep,]) # keep only environments sampled
# subset to material of interest (elite lines and checks, remove control population)
finalData <- droplevels(finalData[which(finalData$type %in% c("Forward", "Checks")),])
with(finalData, table(year, env))
```

The resulting dataset contains ELITE lines and checks evaluated in 4 environments across all the years that the program has been assuming that material at the Elite stage was evaluated with 3 reps in each environment where the plot h2 was 0.2 (you can check the simulation parameters in the RUN_PROGRAM.R script available in the simulated_data folder).

## THE GENETIC GAIN CALCULATION

Once the data has been collected from the different environments for a given germplasm sample, we can proceed to fit a mixed model. In this example we already have the simulated phenotypes and can proceed to the analysis.

### 1) Put the data in good shape

We first define some additional variables as factor to ensure a good estimation of the year and material type effects.

```{r}
finalData$yearf <- as.factor(finalData$year) # convert to factor
finalData$typef <- as.factor(finalData$type) # convert to factor
finalData$fieldinst <- as.factor(paste0(finalData$yearf, finalData$env))
finalData$fieldinstn <- as.numeric(finalData$fieldinst)
finalData <- finalData[with(finalData, order(-fieldinstn)), ]
```

### 2) The mixed model

With the data in good shape we can fit a mixed model of the form:

$y=X\beta+Zu+\epsilon$

where year (as factor and numeric covariate) and the genotype effect are fitted as fixed effects ($\beta$) and year by genotype interaction, year by environment, and environment, as random effects ($u$). The residuals ($\epsilon$) are fitted with a diagonal structure to allow for different residuals at each location.

```{r}
###################
# estimated genetic gain
###################
mix.var <- asreml(p~ year + yearf + env + id,
                  random= ~ yearf:id + env:id,
                  residual=~dsum(~units | fieldinst),
                  data=droplevels(finalData), 
                  maxiter=50, workspace=250000000,trace=FALSE
)
head(summary(mix.var)$varcomp)
```

Looking at the variance components it seems that most of the random effects are different than zero according to the Z.ratio.

### 3) Predict means and merge the year of origin

The program should be able to know what's the year of origin (year when the material was recycled) of the materials. We can use the predict function to obtain across-location-year means for the different genotypes. Notice that not only the same genotypes show in all locations and years, on top of that checks connect the data. This is the reason why the historical data analysis represent a highly accurate estimation of genetic gain.

```{r}
base <- unique(finalData[,c("id","yearOrigin","typef")]) # extract year of origin for materials to merge later
predictions <- predict(mix.var, classify = "id", trace=FALSE)# obtain across years and across environment means
predictions2 <- predictions$pvals # extract adjusted means
predictions3 <- merge(predictions2, base, by="id", all.x = TRUE) # add year of origin to adjusted means
predictions3 <- predictions3[which(predictions3$typef == "Forward"),] # remove checks
head(predictions3)
```

Notice that prediction are linked not only to specific genotypes but now we also have the year of origin of each material.

### 4) The response to selection and rate of gain

This results can be used to calculate the response to selection obtained by year:

```{R}
R0 <- aggregate(predicted.value~yearOrigin,data=predictions3, FUN=mean)
mean(diff(R0$predicted.value)) # mean response per year
R0$predicted.value[nrow(R0)] - R0$predicted.value[1]# total response across years
```
Is more effective if we fit a linear model and use the slope of the regression as the rate of gain per year:

```{r}
mix.var2 <- lm(predicted.value~yearOrigin, data=predictions3) # fit the second model
b2=summary(mix.var2)$coefficients[2,1]; b2 # extract rate of genetic gain
```

As we mentioned at the beggining since we simulated the data we know the real rate of genetic gain:

```{r}
# true genetic gain
realGainData <- droplevels(useData[which(useData$type %in% c("Forward")),]) # filter original data to only forward breeding material (no checks or control population)
realGainData <- aggregate(g~id+yearOrigin,data=realGainData, FUN=mean) # aggregate by year of origin
mix.var.true <- lm(g~yearOrigin, data=realGainData) # fit a model for true genetic gain
b2=summary(mix.var.true)$coefficients[2,1]; b2 # extract true rate of genetic gain
```

## Final remarks

As can be seen the two rates are very similar which shows that a historical data based analysis can be good to estimate the rate of gain. Following Excellence in Breeding recommendations on connectivity of trials using checks in order to be able to use historical data to calculate the rate of gain is the preferred method given the cost and accuracy.

## Literature

Covarrubias-Pazaran G. 2020. Genetic gain as a high level key performance indicator. https://excellenceinbreeding.org/toolbox/tools/eib-breeding-scheme-optimization-manuals 

Walsh, B. 2004. Population-and quantitative-genetic models of selection limits. Plant
breeding reviews 24.1: 177-226.

